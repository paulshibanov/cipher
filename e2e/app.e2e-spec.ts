import { CipherPage } from './app.po';

describe('cipher App', () => {
  let page: CipherPage;

  beforeEach(() => {
    page = new CipherPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
