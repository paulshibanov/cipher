import {Component, OnInit} from '@angular/core';
import {StringEncodingService} from './string-encoding.service';
import {CycleCipherService} from './cycle-cipher.service';
import {ICipher} from './icipher';
import {GammaCipherService} from './gamma-cipher.service';
import {ShuffleCipherService} from './shuffle-cipher.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [StringEncodingService, CycleCipherService, GammaCipherService, ShuffleCipherService]
})
export class AppComponent implements OnInit {
  title = 'app works!';

  cipher: ICipher;
  encoded;
  decoded = '';
  encrypted;
  ciphered;
  decrypted;
  options = [
    {
      id: 0,
      description: 'код циклически смещается на 4 позиции влево',
      method: this.cycleCipherService,
    },
    {
      id: 1,
      description: 'к коду фразы добавляется код гаммы: farbypqsdet',
      method: this.gammaCipherService,
    },
    {
      id: 2,
      description: `в кодовой строке переставляются элементы массива по правилу кратности 2 
      попарно (1-й и 2-й коды меняется с 5-м и 6-м, 3-й и 4-й коды с 7-м и 8-м и т.д.) Если 
      количество кодов не кратно 2, то последние коды остается на своем месте. Полученная строка 
      кодов заменяется символами.`,
      method: this.shuffleCipherService,
    }
  ];
  texts = [
    `Быстро принимаются либо необдуманные, либо очень продуманные решения.`,
    `Нужно уметь быть счастливым не потому, что тебе дают, а потому, что ты даешь.`,
    `Не поддаются лечению те болезни, от которых человек не хочет излечиться`
  ]
  input = this.texts[0];
  constructor(private cycleCipherService: CycleCipherService,
              private gammaCipherService: GammaCipherService,
              private shuffleCipherService: ShuffleCipherService) {
  }
  ngOnInit() {
    this.cipher = this.cycleCipherService;
    this.process();
  }
  process() {
    this.encoded = StringEncodingService.encode(this.input);
    this.encrypted = this.cipher.encrypt(this.encoded);
    this.ciphered = StringEncodingService.decode(this.encrypted);
    this.decrypted = this.cipher.decrypt(this.encrypted);
    this.decoded = StringEncodingService.decode(this.decrypted);
  }
  onCipherChange(option) {
    this.cipher = option.method;
    this.process();
  }
  onTextChange() {
    this.process();
  }
}
