import { TestBed, inject } from '@angular/core/testing';

import { CycleCipherService } from './cycle-cipher.service';

describe('CycleCipherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CycleCipherService]
    });
  });

  it('should ...', inject([CycleCipherService], (service: CycleCipherService) => {
    expect(service).toBeTruthy();
  }));
});
