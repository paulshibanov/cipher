import { Injectable } from '@angular/core';
import { ICipher } from './icipher';

@Injectable()
export class CycleCipherService implements ICipher {

  encrypt(input: number[]): number[] {
    input = input.slice();
    const shift = input.splice(0, 4);
    return input.concat(shift);
  }
  decrypt(input: number[]): number[] {
    input = input.slice();
    const shift = input.splice(-4, 4);
    return shift.concat(input);
  }

}
