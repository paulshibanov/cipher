import { TestBed, inject } from '@angular/core/testing';

import { GammaCipherService } from './gamma-cipher.service';

describe('GammaCipherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GammaCipherService]
    });
  });

  it('should ...', inject([GammaCipherService], (service: GammaCipherService) => {
    expect(service).toBeTruthy();
  }));
});
