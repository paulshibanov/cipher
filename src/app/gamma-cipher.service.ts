import {Injectable} from '@angular/core';
import {ICipher} from './icipher';
import {StringEncodingService} from './string-encoding.service';

@Injectable()
export class GammaCipherService implements ICipher {
  private static GAMMA = 'farbypqsdet';
  private gamma;
  encrypt(input: number[]): number[] {
    return input.map((item, i) => item + this.gamma[i % this.gamma.length]);
  }
  decrypt(input: number[]): number[] {
    return input.map((item, i) => item - this.gamma[i % this.gamma.length]);
  }
  constructor() {
    this.encryptGamma();
  }
  private encryptGamma() {
    this.gamma = StringEncodingService.encode(GammaCipherService.GAMMA);
  }

}
