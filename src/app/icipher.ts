export interface ICipher {
  encrypt(input: number[]): number[];
  decrypt(input: number[]): number[];
}
