import { TestBed, inject } from '@angular/core/testing';

import { ShuffleCipherService } from './shuffle-cipher.service';

describe('ShuffleCipherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShuffleCipherService]
    });
  });

  it('should ...', inject([ShuffleCipherService], (service: ShuffleCipherService) => {
    expect(service).toBeTruthy();
  }));
});
