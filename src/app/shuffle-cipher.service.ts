import {Injectable} from '@angular/core';
import {ICipher} from './icipher';
import {log} from 'util';

@Injectable()
export class ShuffleCipherService implements ICipher {

  encrypt(input: number[]): number[] {
    return this.shuffle(input.slice());
  }
  decrypt(input: number[]): number[] {
    return this.shuffle(input.slice());
  }
  private shuffle(input: number[]): number[] {
    let diff = input.length % 8;
    for (let i = 0; i < input.length - diff; i += 8) {
      for (let j = 0; j < 4; ++ j) {
        swap(input, i + j, i + j + 4);
      }
    }
    for (let i = input.length - 1; diff > 4; --diff, --i) {
      swap(input, i, i - 4);
    }
    return input;
  }
}

export function swap(array: any, i: number, j: number): void {
  [array[i], array[j]] = [array[j], array[i]];
}
