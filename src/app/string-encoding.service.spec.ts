import { TestBed, inject } from '@angular/core/testing';

import { StringEncodingService } from './string-encoding.service';

describe('StringEncodingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StringEncodingService]
    });
  });

  it('should ...', inject([StringEncodingService], (service: StringEncodingService) => {
    expect(service).toBeTruthy();
  }));
});
