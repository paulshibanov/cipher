import { Injectable } from '@angular/core';

@Injectable()
export class StringEncodingService {
  static encode(input: string): number[] {
    return input.split('').map(c => c.charCodeAt(0));
  }
  static decode(input: number[]): string {
    return input.map(i => String.fromCharCode(i)).join('');
  }
}
